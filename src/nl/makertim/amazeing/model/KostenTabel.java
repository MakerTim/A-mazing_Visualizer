package nl.makertim.amazeing.model;

public class KostenTabel {

	private int kapitaal, softwareKosten, verbruikKosten, hardwareKosten;

	private int aanschafKompas, aanschafZWOog, aanschafKleurOog, aanschafVariable;
	private int verbruikStapVooruit, verbruikStapAchteruit, verbruikDraaiLinks, verbruikDraaiRechts, verbruikZWOog,
			verbruikKleurOog, verbruikKompas, verbruikDuw, verbruikToewijzing, verbruikOperatie, verbruikVergelijking;
	private int softwareZolang, softwareAls, softwareOpdracht, softwareToekenning;

	public int getKapitaal() {
		return kapitaal - (getSoftwareKosten() + getVerbruikKosten() + getHardwareKosten());
	}

	public KostenTabel setAanschaf(int aanschafKompas, int aanschafZWOog, int aanschafKleurOog, int aanschafVariable) {
		this.aanschafKompas = aanschafKompas;
		this.aanschafZWOog = aanschafZWOog;
		this.aanschafKleurOog = aanschafKleurOog;
		this.aanschafVariable = aanschafVariable;
		return this;
	}

	public KostenTabel setVerbruik(int verbruikStapVooruit, int verbruikStapAchteruit, int verbruikDraaiLinks, int verbruikDraaiRechts, int verbruikZWOog, int verbruikKleurOog, int verbruikKompas, int verbruikDuw, int verbruikToewijzing, int verbruikOperatie, int verbruikVergelijking) {
		this.verbruikStapVooruit = verbruikStapVooruit;
		this.verbruikStapAchteruit = verbruikStapAchteruit;
		this.verbruikDraaiLinks = verbruikDraaiLinks;
		this.verbruikDraaiRechts = verbruikDraaiRechts;
		this.verbruikZWOog = verbruikZWOog;
		this.verbruikKleurOog = verbruikKleurOog;
		this.verbruikKompas = verbruikKompas;
		this.verbruikDuw = verbruikDuw;
		this.verbruikToewijzing = verbruikToewijzing;
		this.verbruikOperatie = verbruikOperatie;
		this.verbruikVergelijking = verbruikVergelijking;
		return this;
	}

	public KostenTabel setSoftware(int softwareZolang, int softwareAls, int softwareOpdracht, int softwareToekenning) {
		this.softwareZolang = softwareZolang;
		this.softwareAls = softwareAls;
		this.softwareOpdracht = softwareOpdracht;
		this.softwareToekenning = softwareToekenning;
		return this;
	}

	public int getAanschafKompas() {
		return aanschafKompas;
	}

	public KostenTabel setAanschafKompas(int aanschafKompas) {
		this.aanschafKompas = aanschafKompas;
		return this;
	}

	public int getAanschafZWOog() {
		return aanschafZWOog;
	}

	public KostenTabel setAanschafZWOog(int aanschafZWOog) {
		this.aanschafZWOog = aanschafZWOog;
		return this;
	}

	public int getAanschafKleurOog() {
		return aanschafKleurOog;
	}

	public KostenTabel setAanschafKleurOog(int aanschafKleurOog) {
		this.aanschafKleurOog = aanschafKleurOog;
		return this;
	}

	public int getAanschafVariable() {
		return aanschafVariable;
	}

	public KostenTabel setAanschafVariable(int aanschafVariable) {
		this.aanschafVariable = aanschafVariable;
		return this;
	}

	public int getVerbruikStapVooruit() {
		return verbruikStapVooruit;
	}

	public KostenTabel setVerbruikStapVooruit(int verbruikStapVooruit) {
		this.verbruikStapVooruit = verbruikStapVooruit;
		return this;
	}

	public int getVerbruikStapAchteruit() {
		return verbruikStapAchteruit;
	}

	public KostenTabel setVerbruikStapAchteruit(int verbruikStapAchteruit) {
		this.verbruikStapAchteruit = verbruikStapAchteruit;
		return this;
	}

	public int getVerbruikDraaiLinks() {
		return verbruikDraaiLinks;
	}

	public KostenTabel setVerbruikDraaiLinks(int verbruikDraaiLinks) {
		this.verbruikDraaiLinks = verbruikDraaiLinks;
		return this;
	}

	public int getVerbruikDraaiRechts() {
		return verbruikDraaiRechts;
	}

	public KostenTabel setVerbruikDraaiRechts(int verbruikDraaiRechts) {
		this.verbruikDraaiRechts = verbruikDraaiRechts;
		return this;
	}

	public int getVerbruikZWOog() {
		return verbruikZWOog;
	}

	public KostenTabel setVerbruikZWOog(int verbruikZWOog) {
		this.verbruikZWOog = verbruikZWOog;
		return this;
	}

	public int getVerbruikKleurOog() {
		return verbruikKleurOog;
	}

	public KostenTabel setVerbruikKleurOog(int verbruikKleurOog) {
		this.verbruikKleurOog = verbruikKleurOog;
		return this;
	}

	public int getVerbruikKompas() {
		return verbruikKompas;
	}

	public KostenTabel setVerbruikKompas(int verbruikKompas) {
		this.verbruikKompas = verbruikKompas;
		return this;
	}

	public int getVerbruikDuw() {
		return verbruikDuw;
	}

	public KostenTabel setVerbruikDuw(int verbruikDuw) {
		this.verbruikDuw = verbruikDuw;
		return this;
	}

	public int getVerbruikToewijzing() {
		return verbruikToewijzing;
	}

	public KostenTabel setVerbruikToewijzing(int verbruikToewijzing) {
		this.verbruikToewijzing = verbruikToewijzing;
		return this;
	}

	public int getVerbruikOperatie() {
		return verbruikOperatie;
	}

	public KostenTabel setVerbruikOperatie(int verbruikOperatie) {
		this.verbruikOperatie = verbruikOperatie;
		return this;
	}

	public int getVerbruikVergelijking() {
		return verbruikVergelijking;
	}

	public KostenTabel setVerbruikVergelijking(int verbruikVergelijking) {
		this.verbruikVergelijking = verbruikVergelijking;
		return this;
	}

	public int getSoftwareZolang() {
		return softwareZolang;
	}

	public KostenTabel setSoftwareZolang(int softwareZolang) {
		this.softwareZolang = softwareZolang;
		return this;
	}

	public int getSoftwareAls() {
		return softwareAls;
	}

	public KostenTabel setSoftwareAls(int softwareAls) {
		this.softwareAls = softwareAls;
		return this;
	}

	public int getSoftwareOpdracht() {
		return softwareOpdracht;
	}

	public KostenTabel setSoftwareOpdracht(int softwareOpdracht) {
		this.softwareOpdracht = softwareOpdracht;
		return this;
	}

	public int getSoftwareToekenning() {
		return softwareToekenning;
	}

	public KostenTabel setSoftwareToekenning(int softwareToekenning) {
		this.softwareToekenning = softwareToekenning;
		return this;
	}

	public int getSoftwareKosten() {
		return softwareKosten;
	}

	public void addSoftwareKosten(int softwareKosten) {
		this.softwareKosten += softwareKosten;
	}

	public void setSoftwareKosten(int softwareKosten) {
		this.softwareKosten = softwareKosten;
	}

	public int getVerbruikKosten() {
		return verbruikKosten;
	}

	public void addVerbruikKosten(int verbruikKosten) {
		this.verbruikKosten += verbruikKosten;
	}

	public int getHardwareKosten() {
		return hardwareKosten;
	}

	public void addHardwareKosten(int hardwareKosten) {
		this.hardwareKosten += hardwareKosten;
	}

	public void setKapitaal(int start) {
		this.kapitaal = start;
	}

	public void resetKosten() {
		this.softwareKosten = 0;
		this.verbruikKosten = 0;
		this.hardwareKosten = 0;
	}
}
