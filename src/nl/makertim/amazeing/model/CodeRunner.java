package nl.makertim.amazeing.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nl.makertim.amazeing.presentor.GladeWorldPresenter;
import nl.makertim.amazeing.util.CodeHandler20;

public class CodeRunner {

	private transient GladeWorld gw;
	private Map<String, Integer> ram = new HashMap<>();
	private List<String> code;
	private CodeHandler20 ch;
	private boolean zwOog, kleurOog, kompas;

	public void calcSoftwareKosten(KostenTabel kt) {
		int kosten = 0;
		for (String str : code) {
			if (str.contains("zolang")) {
				kosten += kt.getSoftwareZolang();
			} else if (str.contains("als")) {
				kosten += kt.getSoftwareAls();
			} else if (str.contains("stapVooruit") || str.contains("stapAchteruit") || str.contains("draaiLinks")
					|| str.contains("draaiRechts") || str.contains("zwOog") || str.contains("kleurOog")
					|| str.contains("kompas")) {
				kosten += kt.getSoftwareOpdracht();
			} else if (CodeHandler20.isToekenning(str)) {
				kosten += kt.getSoftwareToekenning();
			}
		}
		kt.setSoftwareKosten(kosten);
	}

	public void run(GladeWorld glade) {
		ch = new CodeHandler20(this);
		gw = glade;
		ram.put("", 0);
		ram.put("zwOog", gw.getGriever().getBlok().getZWKleur());
		ram.put("kleurOog", gw.getGriever().getBlok().getKleur());
		ram.put("kompas", gw.getGriever().getRotatie().getValue());
	}

	public boolean step() {
		ram.put("zwOog", gw.getGriever().getBlok().getZWKleur());
		ram.put("kleurOog", gw.getGriever().getBlok().getKleur());
		ram.put("kompas", gw.getGriever().getRotatie().getValue());
		try {
			ch.step();
		} catch (Exception ex) {
			GladeWorldPresenter.getPresenter().openDialog(ex.getClass().getSimpleName(),
					"Line:" + ch.getRow() + "\n" + ex.getMessage());
			return false;
		}
		return true;
	}

	public KostenTabel getKostenTabel() {
		return gw.getKostenTabel();
	}

	public int getRow() {
		return ch.getRow();
	}

	public Map<String, Integer> getRam() {
		return ram;
	}

	public List<String> getCode() {
		return code;
	}

	public void setCode(List<String> newCode) {
		code = newCode;
	}

	public boolean isRunning() {
		return code != null;
	}

	public boolean hasKleurOog() {
		return kleurOog;
	}

	public boolean hasKompas() {
		return kompas;
	}

	public boolean hasZwOog() {
		return zwOog;
	}

	public void setUseKleurOog() {
		this.kleurOog = true;
	}

	public void setUseKompas() {
		this.kompas = true;
	}

	public void setUseZwOog() {
		this.zwOog = true;
	}
}
