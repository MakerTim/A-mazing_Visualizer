package nl.makertim.amazeing.model;

public enum TegelType {
	BOM('B', 0, 8),
	KLEUR('C', 0, 8),
	DOEL('D', 0, 9),
	BONUS('E', 1, 9),
	OBSTAKEL('O', 0, 3),
	DRAAI('R', 0, 3),
	START('S', 0, 3);

	private final char code;
	private final int rangeMin, rangeMax;

	private TegelType(char code, int min, int max) {
		this.code = code;
		this.rangeMax = max;
		this.rangeMin = min;
	}

	public char getCode() {
		return code;
	}

	public int getRangeMax() {
		return rangeMax;
	}

	public int getRangeMin() {
		return rangeMin;
	}

	public boolean isValidRange(int input) {
		return input >= rangeMin && input <= rangeMax;
	}

	public static TegelType fromShort(char c) {
		TegelType ret = null;
		for (TegelType tt : values()) {
			if (tt.getCode() == c) {
				ret = tt;
				break;
			}
		}
		return ret;
	}

}
