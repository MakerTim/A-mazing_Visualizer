package nl.makertim.amazeing.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import nl.makertim.amazeing.factory.BlokFactory;
import nl.makertim.amazeing.factory.GrieverFactory;
import nl.makertim.amazeing.model.Griever.Rotatie;
import nl.makertim.amazeing.presentor.GladeWorldPresenter;
import nl.makertim.amazeing.view.Window;

public class GladeWorld {

	public static final int SIZE = 20;

	private KostenTabel kosten;
	private Griever griever;
	private Blok[][] blokken = new Blok[SIZE][SIZE];
	private Set<Blok> activatedBlokken = new HashSet<>();
	private List<Blok> doelen = new ArrayList<>();

	public GladeWorld() {
		for (int x = 0; x < SIZE; x++) {
			for (int y = 0; y < SIZE; y++) {
				blokken[x][y] = BlokFactory.createNewBlok(TegelType.KLEUR, TegelType.KLEUR.getRangeMax(), x, y);
			}
		}
		kosten = new KostenTabel();
		griever = GrieverFactory.createNewGriever(this, 0, 0, Rotatie.NOORD);
	}

	public KostenTabel getKostenTabel() {
		return kosten;
	}

	public Griever getGriever() {
		return griever;
	}

	public void setBlokken(Blok[][] blokken) {
		if (blokken != null) {
			this.blokken = blokken;
			fixDoelen();
		}
	}

	public void fixDoelen() {
		doelen.clear();
		for (Blok[] blokrij : blokken) {
			for (Blok blok : blokrij) {
				if (blok.getType() == TegelType.DOEL) {
					doelen.add(blok);
				}
			}
		}
	}

	public Blok getBlok(int x, int y) {
		return blokken[x][y];
	}

	public void setGrieverLocation(int x, int y) {
		griever.setLocation(x, y);
	}

	public void setKostenTabel(KostenTabel kostenTabel) {
		this.kosten = kostenTabel;
	}

	public void reset() {
		fixDoelen();
	}

	public void activateBlok(int x, int y) {
		activatedBlokken.add(getBlok(x, y));
	}

	public boolean beatWorld() {
		return doelen.isEmpty();
	}

	public void tick(Window view) {
		Iterator<Blok> blokIt = activatedBlokken.iterator();
		while (blokIt.hasNext()) {
			Blok bl = blokIt.next();
			switch (bl.getType()) {
			case BOM:
				if (bl.getMetadata() < 0) {
					blokIt.remove();
				}
				bl.setMetadata(bl.getMetadata() - 1);
				break;
			case BONUS:
				if (bl.getMetadata() > 0) {
					kosten.addVerbruikKosten(-(int) Math.pow(2, bl.getMetadata()));
					bl.setMetadata(0);
					blokIt.remove();
				}
				break;
			case DOEL:
				if (bl.getMetadata() > 0) {
					int lowestTarget = Integer.MAX_VALUE;
					for (Blok target : doelen) {
						lowestTarget = Math.min(target.getMetadata(), lowestTarget);
					}
					if (bl.getMetadata() == lowestTarget) {
						doelen.remove(bl);
					}
					bl.setMetadata(0);
					blokIt.remove();
				}
				break;
			case DRAAI:
				switch (bl.getMetadata()) {
				case 0:
					switch ((int) (Math.random() * 4)) {
					case 0:
						getGriever().draaiLinks();
						break;
					case 1:
						getGriever().draaiRechts();
						break;
					case 2:
						getGriever().draaiRechts();
						getGriever().draaiRechts();
						break;
					}
					break;
				case 1:
					getGriever().draaiRechts();
					break;
				case 2:
					getGriever().draaiRechts();
					getGriever().draaiRechts();
					break;
				case 3:
					getGriever().draaiLinks();
					break;
				}
				GladeWorldPresenter.getPresenter().updateGrieverRotatie();
			case KLEUR:
			case OBSTAKEL:
			case START:
				blokIt.remove();
				break;
			}
			view.getTile(bl.getX(), bl.getY()).update(bl);
		}
	}

}
