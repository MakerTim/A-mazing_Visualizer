package nl.makertim.amazeing.model;

public interface Blok {

	public TegelType getType();

	public void setType(TegelType newType);

	public int getOriginalMetadata();

	public int getMetadata();

	public void setMetadata(int newMeta);

	public int getKleur();

	public int getZWKleur();

	public int getX();
	
	public int getY();
	
	public boolean isOpaque();
}
