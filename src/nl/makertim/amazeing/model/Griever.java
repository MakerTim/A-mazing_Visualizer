package nl.makertim.amazeing.model;

public interface Griever {

	public enum Rotatie {
		NOORD, OOST, ZUID, WEST;

		public int getValue() {
			switch (this) {
			case NOORD:
				return 0;
			case OOST:
				return 1;
			case WEST:
				return 3;
			case ZUID:
				return 2;
			default:
				return -1;
			}
		}
	}

	public Blok getBlok();

	public int getX();

	public int getY();

	public void setX(int newX);

	public void setY(int newY);

	public Rotatie getRotatie();

	public void draaiLinks();

	public void draaiRechts();

	public void setRotatie(Rotatie rotatie);

	public void setLocation(int x, int y);

}
