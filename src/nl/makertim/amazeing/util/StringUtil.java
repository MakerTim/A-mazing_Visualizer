package nl.makertim.amazeing.util;

public class StringUtil {

	public static int count(String what, char c) {
		int ret = 0;
		for (char whatC : what.toCharArray()) {
			if (whatC == c) {
				++ret;
			}
		}
		return ret;
	}

}
