package nl.makertim.amazeing.util;

import java.util.Map.Entry;

public final class SimpleEntry<K, V> implements Entry<K, V> {
	private final K key;
	private V value;

	public SimpleEntry(K key, V value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public K getKey() {
		return key;
	}

	@Override
	public V getValue() {
		return value;
	}

	@Override
	public V setValue(V value) {
		V old = this.value;
		this.value = value;
		return old;
	}

	@Override
	public String toString() {
		if (getKey() == null || getKey().toString().trim().isEmpty()) {
			return "";
		}
		return String.format("%s:\t%s", getKey().toString(), getValue().toString());
	}
}