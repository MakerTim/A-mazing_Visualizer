package nl.makertim.amazeing.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.makertim.amazeing.model.CodeRunner;
import nl.makertim.amazeing.presentor.GladeWorldPresenter;

public class CodeHandler20 {

	private enum State {
		INIT, PROGRAMMA_BLOK;
	}

	private State state;
	private CodeRunner code;
	private int row;
	private int offset;
	private Boolean anders = null;
	private List<BackToRowHandler> goBackTo = new ArrayList<>();

	public CodeHandler20(CodeRunner cr) {
		code = cr;
		state = State.INIT;
	}

	public void step() {
		if (row >= code.getCode().size()) {
			row++;
			throw new RuntimeException("This script has no errors!");
		} else {
			offset = 0;
			parseRow();
		}
		if (code.getKostenTabel().getKapitaal() < 0) {
			throw new RuntimeException("Kapitaal is negative");
		}
		if (GladeWorldPresenter.getPresenter().hasWon()) {
			throw new RuntimeException("YOU WIN");
		}
	}

	private void parseRow() {
		String line = code.getCode().get(row).trim();
		System.out.printf("CODE EXECUTED:%d \t%s \t", row + 1, line);
		if (line.isEmpty()) {
			row++;
			return;
		}
		if (state == State.INIT) {
			if (line.startsWith("gebruik")) {
				parseGebruik(line);
				System.out.println("gebruik");
			} else {
				state = State.PROGRAMMA_BLOK;
			}
		}
		if (state == State.PROGRAMMA_BLOK) {
			if (line.length() > 1 && isToekenning(line)) {
				parseToekenning(line);
				System.out.println("toekenning");
			} else if (line.startsWith("zolang")) {
				parseZolang(line);
				System.out.println("zolang");
			} else if (line.startsWith("als")) {
				parseAls(line);
				System.out.println("als");
			} else if (line.startsWith("}") && line.contains("anders") && line.contains("{")) {
				parseAnders(line);
				System.out.println("anders");
			} else if (line.startsWith("}")) {
				parseClose(line);
				System.out.println("close");
			} else if (line.startsWith("stapVooruit") || line.startsWith("stapAchteruit")
					|| line.startsWith("draaiLinks") || line.startsWith("draaiRechts")) {
				parseOpdracht(line);
				System.out.println("opdracht");
			} else {
				System.out.println("error");
				throw new RuntimeException("No valid code line");
			}
		}
		row++;
	}

	private void parseToekenning(String line) {
		if (line.matches("[a-z]\\s+=\\s+.+")) {
			String key = line.split("\\s+=\\s+")[0].trim();
			String subLine = line.split("\\s+=\\s+")[1].trim();
			code.getRam().put(key, parseExpressie(subLine));
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikToewijzing());
		} else {
			throw new RuntimeException("not valid toekenning");
		}
	}

	private int parseExpressie(String subLine) {
		if (subLine.matches("\\d+")) {
			int i = Integer.parseInt(subLine);
			if (i >= 0 && i < 65535) {
				return i;
			} else {
				throw new RuntimeException("a integer is not allowed to be " + i);
			}
		} else if (subLine.matches("[a-z]{1}")) {
			if (code.getRam().containsKey(subLine)) {
				return code.getRam().get(subLine);
			} else {
				throw new RuntimeException("variable '" + subLine + "' not found in ram");
			}
		} else if (subLine.equals("kleurOog")) {
			if (code.hasKleurOog()) {
				return code.getRam().get(subLine);
			} else {
				throw new RuntimeException("use kleuroog but has no kleuroog scanner");
			}
		} else if (subLine.equals("zwOog")) {
			if (code.hasZwOog()) {
				return code.getRam().get(subLine);
			} else {
				throw new RuntimeException("use zwOog but has no zwOog scanner");
			}
		} else if (subLine.equals("kompas")) {
			if (code.hasKompas()) {
				return code.getRam().get(subLine);
			} else {
				throw new RuntimeException("use kompas but has no kompas scanner");
			}
		} else if (subLine.matches(".+\\s+[+\\-*/%]\\s+.+")) {
			String[] subs = subLine.split("\\s+");
			if (subs.length % 2 == 1) {
				int ret = -1;
				for (int i = 0; i < subs.length; i += 2) {
					if (ret == -1) {
						ret = parseExpressie(subs[i]);
					} else {
						int x = parseExpressie(subs[i]);
						;
						if (subs[i - 1].equals("+")) {
							ret += x;
						} else if (subs[i - 1].equals("-")) {
							ret -= x;
						} else if (subs[i - 1].equals("*")) {
							ret *= x;
						} else if (subs[i - 1].equals("/")) {
							ret /= x;
						} else if (subs[i - 1].equals("%")) {
							ret %= x;
						} else {
							throw new RuntimeException("sum nested expressie fault");
						}
						code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikOperatie());
					}
				}
				return ret;
			} else {
				throw new RuntimeException("expressie with operatie error");
			}
		} else {
			throw new RuntimeException("fault in expressie");
		}
	}

	private boolean parseVergelijking(String line) {
		code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikVergelijking());
		String[] subs = line.split("\\s+[=<!>]{1,}\\s+");
		if (subs.length == 2) {
			int a = parseExpressie(subs[0]);
			int b = parseExpressie(subs[1]);
			if (line.matches(".+\\s+==\\s+.+")) {
				return a == b;
			} else if (line.matches(".+\\s+!=\\s+.+")) {
				return a != b;
			} else if (line.matches(".+\\s+<\\s+.+")) {
				return a < b;
			} else if (line.matches(".+\\s+>\\s+.+")) {
				return a > b;
			}
			throw new RuntimeException("vergelijking cant figure out what the comparator is");
		} else {
			throw new RuntimeException("vergelijking is not complete");
		}
	}

	private void parseZolang(String line) {
		vergelijking(line, line.replaceAll("zolang\\s+", "").split("\\s+\\{"), true);
	}

	private void parseAls(String line) {
		vergelijking(line, line.replaceAll("als\\s+", "").split("\\s+\\{"), false);
	}

	private void vergelijking(String line, String[] zoals, boolean isZoalsLoop) {
		if (zoals.length == 1 && line.contains("{")) {
			boolean vergelijk = parseVergelijking(zoals[0].trim());
			if (vergelijk || !isZoalsLoop) {
				goBackTo.add(new BackToRowHandler(row, isZoalsLoop));
			}
			if (!vergelijk && isZoalsLoop) {
				goBackTo.add(new BackToRowHandler(row, !isZoalsLoop));
			}
			if (!vergelijk) {
				gotoNextClose();
			}
			anders = !vergelijk;
		} else {
			throw new RuntimeException("missing {");
		}
	}

	private void gotoNextClose() {
		int openBrackets = 1;
		while (openBrackets > 0) {
			if (code.getCode().size() < row + 1) {
				throw new RuntimeException("Brackets are not handled correctly");
			}
			String line = code.getCode().get(++row).trim();
			--offset;
			if (line.contains("}")) {
				--openBrackets;
			} else if (line.contains("{")) {
				++openBrackets;
			}
		}
		++offset;
		--row;
	}

	private void removeLastBack() {
		Iterator<BackToRowHandler> btrhIt = goBackTo.iterator();
		while (btrhIt.hasNext()) {
			BackToRowHandler btrh = btrhIt.next();
			if (!btrhIt.hasNext()) {
				if (btrh.shouldGoBack()) {
					offset = row - btrh.row + 1;
					row = btrh.row - 1;
				}
				btrhIt.remove();
				break;
			}
		}
	}

	private void parseClose(String line) {
		if (line.equals("}")) {
			removeLastBack();
		} else {
			throw new RuntimeException("No code expected after close sign");
		}
	}

	private void parseAnders(String line) {
		if (line.matches("\\}\\s+anders\\s+\\{")) {
			if (anders == null) {
				throw new RuntimeException("Anders not expected");
			} else if (!anders) {
				gotoNextClose();
			} else {
				removeLastBack();
				goBackTo.add(new BackToRowHandler(row, false));
			}
			anders = null;
		} else {
			throw new RuntimeException("Anders statement fault");
		}
	}

	private void parseOpdracht(String line) {
		switch (line) {
		case "stapVooruit":
			GladeWorldPresenter.getPresenter().stapGriever();
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikStapVooruit());
			break;
		case "stapAchteruit":
			GladeWorldPresenter.getPresenter().stapGrieverAchteruit();
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikStapAchteruit());
			break;
		case "draaiLinks":
			GladeWorldPresenter.getPresenter().draaiGrieverLinks();
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikDraaiLinks());
			break;
		case "draaiRechts":
			GladeWorldPresenter.getPresenter().draaiGrieverRechts();
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikDraaiRechts());
			break;
		default:
			throw new RuntimeException("no valid opdracht");
		}
	}

	private void parseGebruik(String line) {
		line = line.replace("gebruik", "").trim();
		switch (line) {
		case "zwOog":
			code.setUseZwOog();
			code.getKostenTabel().addHardwareKosten(code.getKostenTabel().getAanschafZWOog());
			break;
		case "kleurOog":
			code.setUseKleurOog();
			code.getKostenTabel().addHardwareKosten(code.getKostenTabel().getAanschafKleurOog());
			break;
		case "kompas":
			code.setUseKompas();
			code.getKostenTabel().addHardwareKosten(code.getKostenTabel().getAanschafKompas());
			break;
		default:
			if (line.length() == 1) {
				if (code.getRam().containsKey(line)) {
					throw new RuntimeException("variable in ram already in use");
				}
				code.getRam().put(line, 0);
				code.getKostenTabel().addHardwareKosten(code.getKostenTabel().getAanschafVariable());
			} else {
				throw new RuntimeException("gebruik only allows 1 char variables or tools");
			}
			break;
		}
	}

	public int getRow() {
		return row + offset;
	}

	public static boolean isToekenning(String line) {
		return line.contains(" = ") || line.contains(" =\t") || line.contains("\t= ") || line.contains("\t=\t");
	}

	public static class BackToRowHandler {

		private int row;
		private boolean shouldGoBack = false;

		public BackToRowHandler(int row, boolean back) {
			this.row = row;
			this.shouldGoBack = back;
		}

		public int getRow() {
			return this.row;
		}

		public boolean shouldGoBack() {
			return shouldGoBack;
		}

		@Override
		public boolean equals(Object other) {
			if (other instanceof BackToRowHandler) {
				return row == ((BackToRowHandler) other).row;
			}
			return super.equals(other);
		}

		@Override
		public String toString() {
			return (row + 1) + ":" + shouldGoBack;
		}

	}
}
