package nl.makertim.amazeing.view;

import java.util.ArrayList;
import java.util.Map.Entry;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import nl.makertim.amazeing.model.GladeWorld;
import nl.makertim.amazeing.presentor.GladeWorldPresenter;

public class Window extends Application {

	private boolean autoMode;
	private Tile[][] tiles;
	private Stage theStage;
	private TextArea txtUrl, txtCode;
	private ListView<Entry<String, Object>> ramView;
	private Button btnRun, btnStep, btnAutoStep, btnAniStep, btnReset;

	public static void init(String[] args) {
		Platform.setImplicitExit(true);
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		GladeWorldPresenter.initGladeWorldPresenter(this);
		theStage = stage;
		Pane mainPane = new HBox(20);
		Pane leftPane = new VBox(1);
		Pane gamePane = new VBox();

		leftPane.setMinWidth(250);
		ramView = new ListView<>();

		Pane[] rows = new Pane[GladeWorld.SIZE];
		tiles = new Tile[GladeWorld.SIZE][];
		for (int x = 0; x < GladeWorld.SIZE; x++) {
			rows[x] = new HBox();
			tiles[x] = new Tile[GladeWorld.SIZE];
			for (int y = 0; y < GladeWorld.SIZE; y++) {
				rows[x].getChildren().add(tiles[x][y] = new Tile());
			}
		}

		Button btnLoad = new Button("Load");
		btnLoad.setOnMouseClicked(event -> {
			GladeWorldPresenter.getPresenter().updateBlocksByUrl(true);
		});
		HBox scriptPane = new HBox(1);
		btnRun = new Button("Lock");
		btnRun.setOnMouseClicked(event -> {
			GladeWorldPresenter.getPresenter().runCode();
		});
		btnStep = new Button("Step");
		btnStep.setOnMouseClicked(event -> {
			GladeWorldPresenter.getPresenter().stepCode();
		});
		btnStep.setDisable(true);
		btnAutoStep = new Button("AutoStep");
		btnAutoStep.setOnMouseClicked(event -> {
			autoMode(true);
			GladeWorldPresenter.getPresenter().autoStep();
		});
		btnAutoStep.setDisable(true);
		btnAniStep = new Button("Animate");
		btnAniStep.setOnMouseClicked(event -> {
			autoMode(true);
			GladeWorldPresenter.getPresenter().animateStep();
		});
		btnAniStep.setDisable(true);
		btnReset = new Button("Unlock");
		btnReset.setOnMouseClicked(event -> {
			if (autoMode) {
				updateRamList();
				autoMode(false);
				return;
			}
			GladeWorldPresenter.getPresenter().updateBlocksByUrl(false);
			GladeWorldPresenter.getPresenter().resetCode();
		});
		btnReset.setDisable(true);

		txtCode = new TextArea();
		txtCode.setPrefHeight(400);

		scriptPane.getChildren().addAll(btnRun, btnStep, btnAniStep, btnAutoStep, btnReset);
		gamePane.getChildren().addAll(rows);
		leftPane.getChildren().addAll(txtUrl = new TextArea(), btnLoad, txtCode, scriptPane);
		mainPane.getChildren().addAll(leftPane, gamePane, ramView);

		stage.setResizable(false);
		stage.setTitle("A-mazing Visualizer");
		stage.setScene(new Scene(mainPane, 1366, (32 * 21) - 23));
		stage.show();
	}

	public void updateRamList() {
		ramView.setItems(FXCollections.observableList(GladeWorldPresenter.getPresenter().getVariables()));
	}

	public void clearRamList() {
		ramView.setItems(FXCollections.observableList(new ArrayList<>()));
	}

	public void highlightCodeLine(int i) {
		int index = 0, lastIndex = 0;
		for (int times = 0; times < i; times++) {
			lastIndex = index;
			index = (txtCode.getText() + "\n").indexOf("\n", index + 1);
		}
		txtCode.selectRange(lastIndex, index);
	}

	public String getUrl() {
		return txtUrl.getText();
	}

	public void setCode(String code) {
		txtCode.setText(code);
	}

	public void lockCodeblock() {
		highlightCodeLine(0);
		txtCode.setEditable(false);
		btnReset.setDisable(false);
		btnStep.setDisable(false);
		btnAniStep.setDisable(false);
		btnAutoStep.setDisable(false);
		btnRun.setDisable(true);
	}

	public void setResetText(String t) {
		btnReset.setText(t);
	}

	public void autoMode(boolean b) {
		autoMode = b;
		GladeWorldPresenter.getPresenter().setAutoMode(b);
		if (b == true) {
			btnReset.setText("STOP");
		} else {
			btnReset.setText("unlock");
		}
		btnStep.setDisable(b);
		btnAniStep.setDisable(b);
		btnAutoStep.setDisable(b);
	}

	public void unlockCodeblock() {
		txtCode.setEditable(true);
		btnReset.setDisable(true);
		btnStep.setDisable(true);
		btnAniStep.setDisable(true);
		btnAutoStep.setDisable(true);
		btnRun.setDisable(false);
	}

	public String getCode() {
		return txtCode.getText();
	}

	public Tile getTile(int x, int y) {
		return tiles[x][y];
	}

	public void openDialog(String title, String msg) {
		Stage dialog = new Stage();
		dialog.initStyle(StageStyle.UTILITY);
		dialog.initModality(Modality.APPLICATION_MODAL);
		dialog.initOwner(theStage);
		dialog.setTitle(title);

		VBox dialogVbox = new VBox(20);
		dialogVbox.setAlignment(Pos.CENTER);
		Button btnOk = new Button("Ok");
		btnOk.setOnMouseClicked(event -> {
			dialog.close();
		});
		dialogVbox.getChildren().addAll(new Text(msg), btnOk);

		dialog.setScene(new Scene(dialogVbox, 350, 100));
		dialog.show();
	}
}