package nl.makertim.amazeing.view;

import java.util.HashMap;
import java.util.Map;

import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import nl.makertim.amazeing.model.Blok;
import nl.makertim.amazeing.model.Griever;

public class Tile extends Pane {

	private static final int TILE_SIZE = 32;
	private static final Map<String, BackgroundImage> CACHE = new HashMap<>();

	private Griever griever;

	public Tile() {
		setMinSize(TILE_SIZE, TILE_SIZE);
		setBackgroundImage("/resources/white.jpg");
	}

	public void update(Blok blok) {
		if (hasGriever()) {
			return;
		}
		switch (blok.getType()) {
		case BOM:
			switch (blok.getMetadata()) {
			case -1:
				setBackgroundImage("/resources/debris.jpg");
				break;
			case 0:
				setBackgroundImage("/resources/bomb.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/bomb1.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/bomb2.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/bomb3.jpg");
				break;
			case 4:
				setBackgroundImage("/resources/bomb4.jpg");
				break;
			case 5:
				setBackgroundImage("/resources/bomb5.jpg");
				break;
			case 6:
				setBackgroundImage("/resources/bomb6.jpg");
				break;
			case 7:
				setBackgroundImage("/resources/bomb7.jpg");
				break;
			case 8:
				setBackgroundImage("/resources/bomb8.jpg");
				break;
			}
			break;
		case BONUS:
			switch (blok.getMetadata()) {
			case 0:
				setBackgroundImage("/resources/yellow.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/bonus1.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/bonus2.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/bonus3.jpg");
				break;
			case 4:
				setBackgroundImage("/resources/bonus4.jpg");
				break;
			case 5:
				setBackgroundImage("/resources/bonus5.jpg");
				break;
			case 6:
				setBackgroundImage("/resources/bonus6.jpg");
				break;
			case 7:
				setBackgroundImage("/resources/bonus7.jpg");
				break;
			case 8:
				setBackgroundImage("/resources/bonus8.jpg");
				break;
			case 9:
				setBackgroundImage("/resources/bonus9.jpg");
				break;
			}
			break;
		case DOEL:
			switch (blok.getMetadata()) {
			case 0:
				setBackgroundImage("/resources/yellow.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/goal1.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/goal2.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/goal3.jpg");
				break;
			case 4:
				setBackgroundImage("/resources/goal4.jpg");
				break;
			case 5:
				setBackgroundImage("/resources/goal5.jpg");
				break;
			case 6:
				setBackgroundImage("/resources/goal6.jpg");
				break;
			case 7:
				setBackgroundImage("/resources/goal7.jpg");
				break;
			case 8:
				setBackgroundImage("/resources/goal8.jpg");
				break;
			case 9:
				setBackgroundImage("/resources/goal9.jpg");
				break;
			}
			break;
		case DRAAI:
			switch (blok.getMetadata()) {
			default:
			case 0:
				setBackgroundImage("/resources/turn0.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/turn1.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/turn2.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/turn3.jpg");
				break;
			}
			break;
		case KLEUR:
			switch (blok.getMetadata()) {
			case 0:
				setBackgroundImage("/resources/black.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/purple.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/blue.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/green.jpg");
				break;
			case 4:
				setBackgroundImage("/resources/yellow.jpg");
				break;
			case 5:
				setBackgroundImage("/resources/orange.jpg");
				break;
			case 6:
				setBackgroundImage("/resources/red.jpg");
				break;
			case 7:
				setBackgroundImage("/resources/gray.jpg");
				break;
			case 8:
			default:
				setBackgroundImage("/resources/white.jpg");
				break;
			}
			break;
		case OBSTAKEL:
			switch (blok.getMetadata()) {
			case 0:
				setBackgroundImage("/resources/debris.jpg");
				break;
			case 1:
				setBackgroundImage("/resources/bush.jpg");
				break;
			case 2:
				setBackgroundImage("/resources/stone.jpg");
				break;
			case 3:
				setBackgroundImage("/resources/wood.jpg");
				break;
			}
			break;
		case START:
			setBackgroundImage("/resources/black.jpg");
			break;
		default:
			setBackgroundImage("/resources/white.jpg");
			break;
		}
	}

	public void setBackgroundImage(String url) {
		if (!CACHE.containsKey(url)) {
			try {
				CACHE.put(url,
						new BackgroundImage(new Image(url), BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT,
								BackgroundPosition.CENTER,
								new BackgroundSize(TILE_SIZE, TILE_SIZE, false, false, false, false)));
			} catch (Exception ex) {
				System.err.println(url);
			}
		}
		setBackground(new Background(CACHE.get(url)));
	}

	public boolean hasGriever() {
		return griever != null;
	}

	public void removeGriever() {
		if (hasGriever()) {
			Griever griever = this.griever;
			this.griever = null;
			update(griever.getBlok());
		}
	}

	public void setGriever(Griever g) {
		this.griever = g;
		switch (g.getRotatie()) {
		case NOORD:
			setBackgroundImage("/resources/start0.png");
			break;
		case OOST:
			setBackgroundImage("/resources/start1.png");
			break;
		case ZUID:
			setBackgroundImage("/resources/start2.png");
			break;
		case WEST:
			setBackgroundImage("/resources/start3.png");
			break;
		}
	}

	public void updateGriever() {
		setGriever(griever);
	}
}
