package nl.makertim.amazeing.presentor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map.Entry;

import javafx.application.Platform;
import nl.makertim.amazeing.factory.XMLToGladeFactory;
import nl.makertim.amazeing.factory.XMLToGladeFactory.SiteInfoClass;
import nl.makertim.amazeing.model.CodeRunner;
import nl.makertim.amazeing.model.GladeWorld;
import nl.makertim.amazeing.model.Griever;
import nl.makertim.amazeing.model.Griever.Rotatie;
import nl.makertim.amazeing.model.TegelType;
import nl.makertim.amazeing.util.SimpleEntry;
import nl.makertim.amazeing.view.Window;

public class GladeWorldPresenter {

	private static GladeWorldPresenter presenter;

	private Window view;
	private GladeWorld world;
	private CodeRunner code;
	private boolean autoMode = false;

	public static void initGladeWorldPresenter(Window view) {
		presenter = new GladeWorldPresenter(view, new GladeWorld(), new CodeRunner());
	}

	public static GladeWorldPresenter getPresenter() {
		return presenter;
	}

	private GladeWorldPresenter(Window view, GladeWorld world, CodeRunner code) {
		this.view = view;
		this.world = world;
		this.code = code;
	}

	public void openDialog(String title, String msg) {
		Platform.runLater(() -> view.openDialog(title, msg));
	}

	public void updateBlocksByUrl(boolean resetCode) {
		SiteInfoClass siteInfo = XMLToGladeFactory.getBlokkenVanSite(view.getUrl());
		world.setKostenTabel(siteInfo.getKostenTabel());
		world.setBlokken(siteInfo.getBlock());
		if (resetCode) {
			view.setCode(siteInfo.getCode());
		}
		for (int x = 0; x < GladeWorld.SIZE; x++) {
			for (int y = 0; y < GladeWorld.SIZE; y++) {
				view.getTile(x, y).update(world.getBlok(x, y));
				if (world.getBlok(x, y).getType() == TegelType.START) {
					world.getGriever().setRotatie(Rotatie.values()[world.getBlok(x, y).getMetadata()]);
					updateGrieverLocation(x, y);
					updateGrieverRotatie();
				}
			}
		}
		view.updateRamList();
	}

	public void updateGrieverRotatie() {
		view.getTile(world.getGriever().getX(), world.getGriever().getY()).updateGriever();
	}

	public void updateGrieverLocation(int x, int y) {
		view.getTile(world.getGriever().getX(), world.getGriever().getY()).removeGriever();
		world.setGrieverLocation(x, y);
		view.getTile(world.getGriever().getX(), world.getGriever().getY()).setGriever(world.getGriever());

	}

	public List<Entry<String, Object>> getVariables() {
		List<Entry<String, Object>> ret = new ArrayList<>();
		if (code.isRunning()) {
			code.calcSoftwareKosten(world.getKostenTabel());
			ret.add(new SimpleEntry<String, Object>("Kapitaal", world.getKostenTabel().getKapitaal()));
			ret.add(new SimpleEntry<String, Object>("Software", world.getKostenTabel().getSoftwareKosten()));
			ret.add(new SimpleEntry<String, Object>("Hardwar", world.getKostenTabel().getHardwareKosten()));
			ret.add(new SimpleEntry<String, Object>("Verbruik", world.getKostenTabel().getVerbruikKosten()));
		}
		code.getRam().entrySet().forEach(ram -> ret.add(new SimpleEntry<String, Object>(ram.getKey(), ram.getValue())));
		return ret;
	}

	public void draaiGrieverLinks() {
		world.getGriever().draaiLinks();
		updateGrieverRotatie();
		world.activateBlok(world.getGriever().getX(), world.getGriever().getY());
		world.tick(view);
	}

	public void draaiGrieverRechts() {
		world.getGriever().draaiRechts();
		updateGrieverRotatie();
		world.activateBlok(world.getGriever().getX(), world.getGriever().getY());
		world.tick(view);
	}

	private void stapGriever(Griever griever, int relativeX, int relativeY) {
		if (!world.getBlok(griever.getX() + relativeX, griever.getY() + relativeY).isOpaque()) {
			code.getKostenTabel().addVerbruikKosten(code.getKostenTabel().getVerbruikDuw());
		} else {
			updateGrieverLocation(griever.getX() + relativeX, griever.getY() + relativeY);
		}
		world.activateBlok(griever.getX(), griever.getY());
	}

	public void stapGriever() {
		switch (world.getGriever().getRotatie()) {
		case NOORD:
			stapGriever(world.getGriever(), -1, 0);
			break;
		case OOST:
			stapGriever(world.getGriever(), 0, 1);
			break;
		case WEST:
			stapGriever(world.getGriever(), 0, -1);
			break;
		case ZUID:
			stapGriever(world.getGriever(), 1, 0);
			break;
		}
		world.tick(view);
	}

	public void stapGrieverAchteruit() {
		switch (world.getGriever().getRotatie()) {
		case NOORD:
			stapGriever(world.getGriever(), 1, 0);
			break;
		case OOST:
			stapGriever(world.getGriever(), 0, -1);
			break;
		case WEST:
			stapGriever(world.getGriever(), 0, 1);
			break;
		case ZUID:
			stapGriever(world.getGriever(), -1, 0);
			break;
		}
		world.tick(view);
	}

	public boolean hasWon() {
		return world.beatWorld();
	}

	public void runCode() {
		code.run(world);
		code.setCode(Arrays.asList(view.getCode().split("\n")));
		view.lockCodeblock();
		view.updateRamList();
	}

	public boolean stepCode() {
		boolean succesfull = code.step();
		view.highlightCodeLine(code.getRow());
		Platform.runLater(() -> {
			view.updateRamList();
		});
		return succesfull;
	}

	public void autoStep() {
		animatedStep(25);
	}

	public void animateStep() {
		animatedStep(1000);
	}

	public void setAutoMode(boolean autoMode) {
		this.autoMode = autoMode;
	}

	private void animatedStep(long delay) {
		view.clearRamList();
		new Thread(() -> {
			try {
				Thread.sleep(delay);
				while (autoMode && stepCode()) {
					Thread.sleep(delay);
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}).start();
	}

	public void resetCode() {
		code.setCode(null);
		code.getRam().clear();
		world.getKostenTabel().resetKosten();
		view.unlockCodeblock();
		view.updateRamList();
		world.reset();
	}
}
