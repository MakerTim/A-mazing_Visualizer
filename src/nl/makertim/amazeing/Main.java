package nl.makertim.amazeing;

import nl.makertim.amazeing.view.Window;

public final class Main {

	public static void main(String[] args) {
		System.out.println("Starting up!");
		Window.init(args);
	}

}
