package nl.makertim.amazeing.factory;

import nl.makertim.amazeing.model.Blok;
import nl.makertim.amazeing.model.GladeWorld;
import nl.makertim.amazeing.model.Griever;
import nl.makertim.amazeing.model.Griever.Rotatie;

public class GrieverFactory {

	public static Griever createNewGriever(GladeWorld world, int startX, int startY, Rotatie rotatie) {
		return new Griever() {

			private Rotatie r = rotatie;
			private int x = startX;
			private int y = startY;

			@Override
			public Blok getBlok() {
				return world.getBlok(x, y);
			}

			@Override
			public int getX() {
				return x;
			}

			@Override
			public int getY() {
				return y;
			}

			@Override
			public void setX(int newX) {
				x = newX;
			}

			@Override
			public void setY(int newY) {
				y = newY;
			}

			@Override
			public void setLocation(int x, int y) {
				setX(x);
				setY(y);
			}

			@Override
			public Rotatie getRotatie() {
				return r;
			}

			@Override
			public void setRotatie(Rotatie rotatie) {
				r = rotatie;
			}

			@Override
			public void draaiLinks() {
				switch (world.getGriever().getRotatie()) {
				case NOORD:
					r = Rotatie.WEST;
					break;
				case OOST:
					r = Rotatie.NOORD;
					break;
				case WEST:
					r = Rotatie.ZUID;
					break;
				case ZUID:
					r = Rotatie.OOST;
					break;
				}
			}

			@Override
			public void draaiRechts() {
				switch (world.getGriever().getRotatie()) {
				case NOORD:
					r = Rotatie.OOST;
					break;
				case OOST:
					r = Rotatie.ZUID;
					break;
				case WEST:
					r = Rotatie.NOORD;
					break;
				case ZUID:
					r = Rotatie.WEST;
					break;
				}
			}
		};
	}
}
