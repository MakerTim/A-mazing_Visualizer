package nl.makertim.amazeing.factory;

import nl.makertim.amazeing.model.Blok;
import nl.makertim.amazeing.model.TegelType;

public class BlokFactory {

	public static Blok createNewBlok(TegelType tegelType, int tegelMeta, int xPos, int yPos) {
		return new Blok() {

			private TegelType type = tegelType;
			private final int Ometa = tegelMeta;
			private int meta = tegelMeta;

			private final int x = xPos, y = yPos;

			@Override
			public TegelType getType() {
				return type;
			}

			@Override
			public int getMetadata() {
				return meta;
			}

			@Override
			public void setType(TegelType newType) {
				type = newType;
			}

			@Override
			public void setMetadata(int newMeta) {
				meta = newMeta;
			}

			@Override
			public int getKleur() {
				int ret;
				switch (tegelType) {
				default:
				case BOM:
					ret = 0;
					break;
				case BONUS:
					ret = 4;
					break;
				case DOEL:
					ret = 4;
					break;
				case DRAAI:
					ret = 2;
					break;
				case KLEUR:
					ret = getMetadata();
					break;
				case OBSTAKEL:
					ret = 0;
					break;
				case START:
					ret = 0;
					break;
				}
				return ret;
			}

			@Override
			public int getZWKleur() {
				return getKleur() == 0 ? 0 : 1;
			}

			@Override
			public boolean isOpaque() {
				switch (getType()) {
				case BOM:
					return getMetadata() > -1;
				case BONUS:
					return true;
				case DOEL:
					return true;
				case DRAAI:
					return true;
				case KLEUR:
					return true;
				case OBSTAKEL:
					return false;
				case START:
					return true;
				}
				return true;
			}

			@Override
			public int getX() {
				return x;
			}
			
			@Override
			public int getY() {
				return y;
			}
			
			@Override
			public int getOriginalMetadata() {
				return Ometa;
			}

			@Override
			public String toString() {
				return getType() + ":" + getMetadata();
			}
		};
	}
}
