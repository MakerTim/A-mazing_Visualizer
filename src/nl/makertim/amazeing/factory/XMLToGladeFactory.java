package nl.makertim.amazeing.factory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Optional;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import nl.makertim.amazeing.model.Blok;
import nl.makertim.amazeing.model.GladeWorld;
import nl.makertim.amazeing.model.KostenTabel;
import nl.makertim.amazeing.model.TegelType;
import nl.makertim.amazeing.presentor.GladeWorldPresenter;

public class XMLToGladeFactory {

	private static final String MAZE_ARRAY_KEY = "loadMaze";

	public static SiteInfoClass getBlokkenVanSite(String html) {
		Blok[][] ret = null;
		String code = "";
		KostenTabel kt = new KostenTabel();
		try {
			Document doc = Jsoup.parse(html);

			// KostenTabel
			fillKostenTabel(kt, doc);

			// Get old code, if there is
			Element codeBlock = doc.getElementById("programCodeArea");
			if (codeBlock != null) {
				code = codeBlock.ownText();
			}

			// Get Script block of website
			Element javaScriptBlock = doc.select("script").last();
			if (javaScriptBlock == null) {
				throw new IOException("No valid page - Javascript missing");
			}
			String[] javascript = javaScriptBlock.data().split("\n");
			Optional<String> optionalMaze = Arrays.stream(javascript).filter(line -> line.startsWith(MAZE_ARRAY_KEY))
					.findFirst();
			if (!optionalMaze.isPresent()) {
				throw new IOException("No maze found");
			}

			// Parse maze to java
			String[] maze = optionalMaze.get().split("\"")[1].split("\"")[0].split(";");
			ret = new Blok[GladeWorld.SIZE][GladeWorld.SIZE];
			int i = 0;
			for (String mazeShort : maze) {
				ret[i / 20][i % 20] = BlokFactory.createNewBlok(TegelType.fromShort(mazeShort.charAt(0)),
					Integer.parseInt(Character.toString(mazeShort.charAt(1))), i / 20, i % 20);
				i++;
			}
		} catch (Exception ex) {
			GladeWorldPresenter.getPresenter().openDialog(ex.getClass().getSimpleName(), ex.getMessage());
			ex.printStackTrace();
		}
		return new SiteInfoClass(ret, code, kt);
	}

	private static void fillKostenTabel(KostenTabel kt, Document doc) {
		Elements inputs = doc.select(".table input");
		for (Element input : inputs) {
			switch (input.attr("name")) {
			case "compassHardware":
				kt.setAanschafKompas(Integer.parseInt(input.attr("value")));
				break;
			case "stepForwardUse":
				kt.setVerbruikStapVooruit(Integer.parseInt(input.attr("value")));
				break;
			case "whileLoopSoftware":
				kt.setSoftwareZolang(Integer.parseInt(input.attr("value")));
				break;
			case "bwEyeHardware":
				kt.setAanschafZWOog(Integer.parseInt(input.attr("value")));
				break;
			case "stepBackwardsUse":
				kt.setVerbruikStapAchteruit(Integer.parseInt(input.attr("value")));
				break;
			case "ifChoiceSoftware":
				kt.setSoftwareAls(Integer.parseInt(input.attr("value")));
				break;
			case "colorEyeHardware":
				kt.setAanschafKleurOog(Integer.parseInt(input.attr("value")));
				break;
			case "turnLeftUse":
				kt.setVerbruikDraaiLinks(Integer.parseInt(input.attr("value")));
				break;
			case "assignmentSoftware":
				kt.setSoftwareOpdracht(Integer.parseInt(input.attr("value")));
				break;
			case "variableAzHardware":
				kt.setAanschafVariable(Integer.parseInt(input.attr("value")));
				break;
			case "turnRightUse":
				kt.setVerbruikDraaiRechts(Integer.parseInt(input.attr("value")));
				break;
			case "assignSoftware":
				kt.setSoftwareToekenning(Integer.parseInt(input.attr("value")));
				break;
			case "bwEyeUse":
				kt.setVerbruikZWOog(Integer.parseInt(input.attr("value")));
				break;
			case "colorEyeUse":
				kt.setVerbruikKleurOog(Integer.parseInt(input.attr("value")));
				break;
			case "compassUse":
				kt.setVerbruikKompas(Integer.parseInt(input.attr("value")));
				break;
			case "pushObstacleUse":
				kt.setVerbruikDuw(Integer.parseInt(input.attr("value")));
				break;
			case "assignUse":
				kt.setVerbruikToewijzing(Integer.parseInt(input.attr("value")));
				break;
			case "operationUse":
				kt.setVerbruikOperatie(Integer.parseInt(input.attr("value")));
				break;
			case "equalUse":
				kt.setVerbruikVergelijking(Integer.parseInt(input.attr("value")));
				break;
			case "startCredits":
				kt.setKapitaal(Integer.parseInt(input.attr("value")));
				break;
			default:
				System.err.println("MISSING CASE IN KOSTENDIAGRAM - " + input.attr("name"));
				break;
			}
		}
	}

	public static class SiteInfoClass {
		private Blok[][] block;
		private String code;
		private KostenTabel kt;

		public SiteInfoClass(Blok[][] block, String code, KostenTabel kt) {
			this.block = block;
			this.code = code;
			this.kt = kt;
		}

		public Blok[][] getBlock() {
			return block;
		}

		public String getCode() {
			return code;
		}

		public KostenTabel getKostenTabel() {
			return kt;
		}
	}
}
